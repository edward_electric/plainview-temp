1.1 2019-01-09

- New: Show filename in URL
- Fix: Force text/plain for anything that could be a text file.
- Fix: Do not filter uploaded text files.
- UI: Use lock icon on private directories.

1.0 2019-01-05

- Initial release.
