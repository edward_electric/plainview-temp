<?php

namespace App\Http\Controllers;

use App\Models\Files\Directory;
use App\Models\Files\File;
use ZipArchive;

/**
	@brief		Show public files.
	@since		2018-12-31 15:47:13
**/
class Public_Files
	extends Controller
{
	/**
		@brief		GET the page.
		@since		2018-12-31 15:30:56
	**/
	public function get()
	{
		$temp_item = app()->temp_item;

		$mime = mime_content_type( $temp_item->get_path_and_filename() );

		// If this is a text file, force it to text.
		if ( strpos( $mime, 'text/' ) === 0 )
			$mime = 'text/plain';

		if ( $temp_item->is_file() )
			return response()->file( $temp_item->get_path_and_filename(), [
				'Content-Length' => filesize( $temp_item->get_path_and_filename() ),
				'Content-Type' => $mime,
				'Content-Disposition' => 'inline;filename="' . $temp_item->get_filename() . '"',
				] );

		return view( 'files.index', [
			'files' => $temp_item,
			'forms' => $this->get_forms(),
			'title' => $temp_item->get_browsable_path(),
		] );
	}

	/**
		@brief		The form for directories.
		@since		2019-01-05 02:17:00
	**/
	public function get_directory_form()
	{
		$form = app()->form();
		$form->icon = 'fa-folder-plus';
		$form->human_name = 'Create directory';
		$form->id( 'directory_form' );

		$dirname = md5( microtime() );
		$dirname = substr( $dirname, 0, 8 );
		$form->directory = $form->text( 'directory' )
			->description( __( 'Create a new private directory with this name.' ) )
			->label( __( 'New directory name' ) )
			->trim()
			->value( $dirname );

		$form->create_directory = $form->submit( 'create_directory' )
			->value( __( 'Create directory' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		Return an array of all forms.
		@since		2019-01-05 02:17:31
	**/
	public function get_forms()
	{
        $r = (object) [
			'upload' => $this->get_upload_form(),
			'text' => $this->get_text_form(),
			'directory' => $this->get_directory_form(),
		];

        return $r;
	}

	/**
		@brief		Return the name of the route of the section (public, private) we are in.
		@since		2019-01-05 00:27:25
	**/
	public function get_route()
	{
		return 'public_get';
	}

	/**
		@brief		The form for text files.
		@since		2019-01-05 02:17:00
	**/
	public function get_text_form()
	{
		$form = app()->form();
		$form->icon = 'fa-pen';
		$form->human_name = 'Paste text';
		$form->id( 'text_form' );

		$form->text_name = $form->text( 'text_name' )
			->label( __( 'Text file name' ) )
			->trim()
			->value( 'Text ' . date( 'Y-m-d H:i:s' ) );

		$form->text_content = $form->textarea( 'text_content' )
			->label( __( 'Content' ) )
			->rows( 5, 40 )
			->trim();

		$form->create_text_file = $form->submit( 'create_text_file' )
			->value( __( 'Create text file' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		get_upload_form
		@since		2019-01-05 02:16:39
	**/
	public function get_upload_form()
	{
		$form = app()->form();
		$form->icon = 'fa-upload';
		$form->human_name = 'Upload';
		$form->id( 'upload_form' );

		$form->file = $form->file( 'file' )
			->label( __( 'File to upload' ) );

		$form->upload = $form->submit( 'upload' )
			->value( __( 'Upload file' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		POST the page.
		@since		2018-12-31 15:30:56
	**/
	public function post()
	{
		try
		{
			$forms = $this->get_forms();
			$temp_item = app()->temp_item;

			if ( isset( $_POST[ 'delete_files' ] ) )
			{
				$delete_files = $_POST[ 'delete_files' ];
				foreach( $delete_files[ 'files_to_delete' ] as $filename )
				{
					$filename = File::normalize( $filename );
					$temp_item->unlink( $filename );
				}
				return response()->json([
					'result' => 'ok',
				]);
			}

			if ( isset( $_POST[ 'zip_files' ] ) )
			{
				$files_to_zip = $_POST[ 'zip_files' ][ 'files_to_zip' ];

				$zip_filename = sprintf( "zip_%s.zip",
					$temp_item->get_filename()
				);
				$zip_path = sprintf( "%s%s%s",
					$temp_item->get_path_and_filename(),
					DIRECTORY_SEPARATOR,
					$zip_filename
				);
				$temp_item->unlink( $zip_filename );
				chdir( $temp_item->get_path() );

				$zip = new ZipArchive();
				$zip->open( $zip_filename,  ZipArchive::CREATE);
				foreach( $files_to_zip as $file_to_zip )
				{
					if ( ! is_readable( $file_to_zip ) )
						continue;
					$zip->addFile( $file_to_zip );
				}
				$zip->close();

				return response()->json([
					'result' => 'ok',
				]);
			}

			if ( $forms->directory->create_directory->pressed() )
			{
				$directory_name = $forms->directory->directory->get_filtered_post_value();
				$directory_name = Directory::normalize( $directory_name );
				app()->log()->info( 'Creating directory: %s', $directory_name );
                return redirect()->route( 'private_get', [ $directory_name ] );
			}

			if ( isset( $forms->public ) )
                if ( $forms->public->back_to_public->pressed() )
                    return redirect( route( 'index' ) );

			if ( $forms->upload->upload->pressed() )
			{
				$uploaded_file = $forms->upload->file->get_post_value();
				$suffix = '1';
                $free_upload_file_path = $temp_item->get_path( File::normalize( $uploaded_file->name ) );
				while ( file_exists( $free_upload_file_path ) )
				{
					$suffix++;
					$free_upload_file_path = $free_upload_file_path . '.' . $suffix;
				}
				copy( $uploaded_file->tmp_name, $free_upload_file_path );
				app()->log()->info( 'Uploaded %s', $free_upload_file_path );
			}

			if ( $forms->text->create_text_file->pressed() )
			{
				$filename = $forms->text->text_name->get_filtered_post_value();
				if ( $filename == '' )
					$filename = date( 'Y-m-d H:i:s' );
				$filename .= '.txt';
				$filename = File::normalize( $filename );
				$content = $forms->text->text_content->get_post_value();
				$text_filename = $temp_item->get_path() . DIRECTORY_SEPARATOR . $filename;
				file_put_contents( $text_filename, $content );
				app()->log()->info( 'Created text file: %s', $text_filename );
			}

			return redirect( $temp_item->get_url() );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->alert();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
