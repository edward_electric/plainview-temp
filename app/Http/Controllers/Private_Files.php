<?php

namespace App\Http\Controllers;

use App\Models\Files\Directory;

/**
	@brief		Handle private files.
	@since		2019-01-05 00:08:41
**/
class Private_Files
	extends Public_Files
{
	/**
		@brief		Create a new private directory at random.
		@since		2019-01-05 00:08:15
	**/
	public function create()
	{
		do
		{
			$dirname = md5( microtime() );
			$dirname = substr( $dirname, 0, 8 );
		}
		while( $this->directory_exists( $dirname ) );

		app()->log()->info( 'Creating private directory: %s', $dirname );

		return redirect( route( 'private_get', [ $dirname ] ) );
	}

	/**
		@brief		Does this private directory exist?
		@since		2021-01-17 14:33:44
	**/
	public function directory_exists( $dirname )
	{
		return file_exists( Directory::get_private_storage_path( $dirname ) );
	}

	public function get_directory( $dirname )
	{
		$path = Directory::get_private_storage_path( $dirname );
		$item = Directory::from( dirname( $private_directory ) );
	}

	/**
		@brief		Return the name of the route of the section (public, private) we are in.
		@since		2019-01-05 00:27:25
	**/
	public function get_route()
	{
		return 'private_get';
	}

	/**
		@brief		Redirect to this private directory.
		@since		2021-01-17 14:35:24
	**/
	public function redirect_to( $dirname )
	{
		$private_directory = Directory::get_private_storage_path( $dirname );
		$item = Directory::from( $private_directory );
		$item->set_private();
		$item->filename = '';
		return redirect( $item->get_url() );
	}
}
