<?php

namespace App\Http\Middleware;

use App\Models\Files\File;
use App\Models\Files\Directory;
use Closure;
use Illuminate\Support\Facades\Request;

/**
	@brief		Check for the existence of this file in storage.
	@since		2018-12-31 16:32:50
**/
class File_Exists
{
    /**
    	@brief		Check the route segments for existence in the $type storage dir.
    	@since		2018-12-31 16:33:12
    **/
    public static function check_file( $type, $path )
    {
    	$is_private = ( $type == 'private' );

    	$segments = explode( DIRECTORY_SEPARATOR, $path );
    	$segments = array_filter( $segments );

    	// Remove the first public or private part.
    	array_shift( $segments );

    	// Clean up the segments from .. and similar hacks.
    	foreach( $segments as $index => $segment )
    	{
    		$segments[ $index ] = File::normalize( $segment );
    		$segments[ $index ] = rawurldecode( $segments[ $index ] );
    	}

		// We are not allowed to view the base private dir.
		if ( $type == 'private' )
			if ( count( $segments ) < 1 )
				return false;

		// Does this path exist?
		$path = implode( DIRECTORY_SEPARATOR, $segments );

		if ( $is_private )
			$path_to_check = Directory::get_private_storage_path( $path );
		else
			$path_to_check = Directory::get_public_storage_path( $path );

		if ( ! file_exists( $path_to_check ) )
		{
			if ( $is_private )
				mkdir( $path_to_check, 0777, true );
			else
				return false;
		}

		if ( is_dir( $path_to_check ) )
			$item = Directory::from( $path_to_check );
		if ( is_file( $path_to_check ) )
			$item = File::from( $path_to_check );

		if ( $is_private )
			$item->set_private();

		return $item;
    }

    /**
    	@brief		Check this file for existence.
    	@since		2022-07-05 23:33:35
    **/
    public function do_check( $type )
    {
		$path = Request::path();
    	return static::check_file( $type, $path );
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$result = $this->do_check( $this->get_type() );
    	if ( ! $result )
    		return redirect( route( 'public_get', [ '' ] ) );
    	app()->temp_item = $result;
        return $next( $request );
    }
}
