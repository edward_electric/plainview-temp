<?php

namespace App\Http\Middleware;

use Closure;

class Private_Exists
	extends File_Exists
{
	/**
		@brief		Return the type of existence check.
		@since		2019-01-04 19:09:52
	**/
	public function get_type()
	{
		return 'private';
	}
}
