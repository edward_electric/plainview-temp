<?php

namespace App\Http\Middleware;

use Closure;

/**
	@brief		Does this file exist in public storage?
	@since		2018-12-31 16:32:09
**/
class Public_Exists
	extends File_Exists
{
	/**
		@brief		Return the type of existence check.
		@since		2019-01-04 19:09:52
	**/
	public function get_type()
	{
		return 'public';
	}
}
