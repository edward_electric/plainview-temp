<?php

namespace App\Http\Middleware;

use App\Models\Files\File;
use App\Models\Files\Directory;
use Illuminate\Support\Facades\Storage;

use Closure;

/**
	@brief		Clean up old files.
	@since		2019-01-04 23:19:06
**/
class Cleanup
{
	/**
		@brief		Clean up a base directory.
		@since		2019-01-04 23:20:02
	**/
	public function clean( $base )
	{
		$old_age = config( 'temp.file_delete_age' );
		$path = storage_path( 'app/' . $base );
		$files = glob( $path . DIRECTORY_SEPARATOR . '*' );
		foreach( $files as $file )
		{
			if ( time() - filemtime( $file ) < $old_age )
				continue;
			static::recursive_delete( $file );
		}
	}

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$this->clean( 'public' );
    	$this->clean( 'private' );
        return $next( $request );
    }

    /**
    	@brief		Recursive delete this file / directory.
    	@since		2019-01-04 23:24:01
    **/
    public function recursive_delete( $path )
    {
    	// This is a directory.
    	if ( is_dir( $path ) )
    	{
    		$files = glob( $path . DIRECTORY_SEPARATOR . '{,.}[!.,!..]*',GLOB_MARK|GLOB_BRACE);
    		foreach( $files as $file )
    			static::recursive_delete( $file );
    		rmdir( $path );
    	}
    	else
	    	unlink( $path );
    }
}
