<?php

namespace App\Classes;

/**
	@brief		UI alerts.
	@since		2018-07-12 22:48:30
**/
class Alerts
	extends \App\Classes\Collections\Collection
{
	/**
		@brief		Add this alert.
		@since		2018-07-12 23:13:32
	**/
	public function add_alert( $alert )
	{
		$this->append( $alert );
		$this->save();
		return $alert;
	}

	/**
		@brief		Add an alert.
		@since		2018-07-12 22:56:51
	**/
	public function alert()
	{
		return $this->add_alert( new \App\Models\Alerts\Alert() );
	}

	/**
		@brief		Display all alerts.
		@since		2018-07-12 22:59:21
	**/
	public function display()
	{
		foreach( $this as $index => $alert )
		{
			$this->forget( $index );
			echo $alert->get_html();
		}
		$this->save();
	}

	/**
		@brief		Add a info alert.
		@since		2018-07-12 22:56:51
	**/
	public function info()
	{
		return $this->add_alert( new \App\Models\Alerts\Info() );
	}

	/**
		@brief		Load the stored alerts from the session.
		@since		2018-07-12 23:03:04
	**/
	public function load_from_session()
	{
		$items = session( 'alerts_items', [] );
		$this->import_array( $items );
	}

	/**
		@brief		Save the alerts to the session.
		@since		2018-07-12 23:02:39
	**/
	public function save()
	{
		session( [ 'alerts_items' => $this->items ] );
	}

	/**
		@brief		Add a secondary alert.
		@since		2018-07-12 22:56:51
	**/
	public function secondary()
	{
		return $this->add_alert( new \App\Models\Alerts\Secondary() );
	}

	/**
		@brief		Add a success alert.
		@since		2018-07-12 22:56:51
	**/
	public function success()
	{
		return $this->add_alert( new \App\Models\Alerts\Success() );
	}

	/**
		@brief		Add a warning alert.
		@since		2018-07-12 22:56:51
	**/
	public function warning()
	{
		return $this->add_alert( new \App\Models\Alerts\Warning() );
	}

}
