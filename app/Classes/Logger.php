<?php

namespace App\Classes;

/**
	@brief		Convenience class to interact with the logger, but sprintfd.
	@since		2018-12-05 17:13:27
**/
class Logger
{
	/**
		@brief		Do the logging.
		@since		2018-12-05 17:17:00
	**/
	public function do_log( $type, array $args )
	{
		$string = reset( $args );
		// Ignore "too few arguments"
		$converted = @call_user_func_array( 'sprintf', $args );
		if ( ! $converted )
			$converted = $string;
		$this->get_logger()->$type( $converted );
	}

	/**
		@brief		Log a debug message.
		@since		2018-12-05 17:16:00
	**/
	public function debug()
	{
		return $this->do_log( 'debug', func_get_args() );
	}

	/**
		@brief		Log an error message.
		@since		2018-12-05 17:16:00
	**/
	public function error()
	{
		return $this->do_log( 'error', func_get_args() );
	}

	/**
		@brief		Return the logger facade.
		@since		2018-12-05 17:16:04
	**/
	public function get_logger()
	{
		return \Illuminate\Support\Facades\Log::getLogger();
	}

	/**
		@brief		Log an info message.
		@since		2018-12-05 17:16:00
	**/
	public function info()
	{
		return $this->do_log( 'info', func_get_args() );
	}

	/**
		@brief		Log a notice message.
		@since		2018-12-05 17:16:00
	**/
	public function notice()
	{
		return $this->do_log( 'notice', func_get_args() );
	}

	/**
		@brief		Log an warning message.
		@since		2018-12-05 17:16:00
	**/
	public function warning()
	{
		return $this->do_log( 'warning', func_get_args() );
	}
}
