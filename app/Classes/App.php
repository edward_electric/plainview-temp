<?php

namespace App\Classes;

class App
	extends \Illuminate\Foundation\Application
{
	/**
		@brief		Return the alerts instance.
		@since		2018-11-28 14:40:20
	**/
	public function alerts()
	{
		$r = new \App\Classes\Alerts();
		$r->load_from_session();
		return $r;
	}

	/**
		@brief		Return a collection.
		@since		2021-01-17 12:35:45
	**/
	public function collection()
	{
		return new \plainview\sdk\collections\collection();
	}

    /**
    	@brief		Return a Plainview Form object.
    	@since		2018-11-13 13:09:48
    **/
    public static function form()
    {
    	$form = new \plainview\sdk\form2\bootstrap4();
		$form->enctype( 'multipart/form-data' );
		return $form;
    }

	/**
		@brief		Return the logger.
		@since		2018-11-16 16:57:59
	**/
	public function log()
	{
		return new Logger();
	}
}
