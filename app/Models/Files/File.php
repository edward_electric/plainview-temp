<?php

namespace App\Models\Files;

/**
	@brief		An individual file in a directory.
	@since		2018-12-31 16:39:00
**/
class File
	extends Item
{
	/**
		@brief		Does this item exist on disk?
		@since		2022-07-11 06:30:01
	**/
	public function exists()
	{
		return file_exists( $this->get_path_and_filename() );
	}

	/**
		@brief		Create a file from this path.
		@since		2019-01-04 20:53:35
	**/
	public static function from( $path )
	{
		$file = new File();
		$file->from = $path;
		$file->path = dirname( $path );
		$file->filename = basename( $path );
		$file->filename = static::normalize( $file->filename );

		$storage_path = Directory::get_private_storage_path( basename( $file->path ) );
		$file->private = ( strpos( $file->path, $storage_path ) === 0 );
		return $file;
	}

	/**
		@brief		Return a checkbox input for this file.
		@since		2022-07-06 21:58:17
	**/
	public function get_checkbox()
	{
		$form = app()->form();
		$cb = $form->checkbox( md5( $this->get_filename() ) )
			->label( __( 'Select %s', [ $this->get_filename() ] ) );
		return $cb->display_input();
	}

	/**
		@brief		Return the URL for this item.
		@since		2019-01-05 00:29:17
	**/
	public function get_url()
	{
		$route = $this->get_route();
		$public_path = $this->get_browsable_path();
		$filename = $this->get_filename();
		$parameter = $public_path . DIRECTORY_SEPARATOR . rawurlencode( $filename );
		$parameter = trim( $parameter, DIRECTORY_SEPARATOR );
		return route( $route, [ $parameter ] );
	}

	/**
		@brief		Is this a directory?
		@since		2018-12-31 16:40:29
	**/
	public function is_directory()
	{
		return false;
	}

	/**
		@brief		Is this a file?
		@since		2018-12-31 16:40:29
	**/
	public function is_file()
	{
		return true;
	}

	/**
		@brief		Is this an image?
		@since		2021-01-16 21:45:35
	**/
	public function is_image()
	{
		$mime = mime_content_type( $this->path . '/' . $this->filename );
		return ( strpos( $mime, 'image/' ) === 0 );
	}

	/**
		@brief		Normalize the filename, removing dangerous things.
		@details	Leaves the spaces in filenames.
		@since		2022-07-05 22:57:34
	**/
	public static function normalize( $string )
	{
		$string = str_replace( '..', '', $string );
		$string = str_replace( DIRECTORY_SEPARATOR, '', $string );
		$string = trim( $string );
		return $string;
	}

	/**
		@brief		Unlink this file.
		@since		2022-07-06 16:35:53
	**/
	public function unlink()
	{
		unlink( $this->get_path_and_filename() );
	}
}
