<?php

namespace App\Models\Files;

/**
	@brief		A dir in a dir.
	@since		2018-12-31 16:40:14
**/
class Directory
	extends Item
{
	/**
		@brief		Create a directory here.
		@since		2019-01-03 20:27:52
	**/
	public function create_directory( $dir )
	{
		$dirname = File::normalize( $dir );
		$path = $this->get_path_and_filename() . DIRECTORY_SEPARATOR . $dirname;
		if ( is_dir( $path ) )
			return;
		mkdir( $path );
	}

	/**
		@brief		Does this item exist on disk?
		@since		2022-07-11 06:30:01
	**/
	public function exists()
	{
		return is_dir( $this->get_path_and_filename() );
	}

	/**
		@brief		Load the items from this directory.
		@since		2019-01-03 15:50:33
	**/
	public static function from( $directory )
	{
		$dir = new Directory();
		$dir->from = $directory;
		$dir->path = $directory;

		$dir->filename = basename( $dir->from );
		$dir->filename = static::normalize( $dir->filename );

		$storage_path = Directory::get_private_storage_path( $dir->filename );
		$dir->private = ( strpos( $dir->path, $storage_path ) === 0 );

		$public_path = $dir->get_browsable_path();
		$segments = explode( DIRECTORY_SEPARATOR, trim( $public_path, '/' ) );

		$files = glob( $directory . '/*' );
		foreach( $files as $file )
		{
			// Don't bother with directories.
			if ( is_dir( $file ) )
				continue;

			$item = new File();
			$item->filename = basename( $file );
			$item->filesize = filesize( $file );
			$item->path = $directory;

			if ( $dir->is_private() )
				$item->set_private();

			$dir->append( $item );
		}

		return $dir;
	}

	/**
		@brief		Return a collection of all images in this directory.
		@since		2021-01-17 12:34:21
	**/
	public function get_images()
	{
		$r = app()->collection();
		foreach( $this as $item )
			if ( $item->is_image() )
				$r->append( $item );
		return $r;
	}

	/**
		@brief		Return an array of image URLs.
		@since		2021-01-17 13:08:28
	**/
	public function get_image_urls()
	{
		$r = [];
		foreach( $this->get_images() as $image )
			$r []= $image->get_url();
		return $r;
	}

	/**
		@brief		Return the disk path + filename.
		@since		2019-01-04 19:04:32
	**/
	public function get_path_and_filename()
	{
		return $this->get_path();
	}

	/**
		@brief		Return the private storage directory on disk.
		@since		2022-07-05 23:18:50
	**/
	public static function get_private_storage_path( $extra_path = '' )
	{
		if ( $extra_path != '' )
			$extra_path = DIRECTORY_SEPARATOR . $extra_path;
		return static::get_storage_path( 'private' ) . $extra_path;
	}

	/**
		@brief		Return the public storage directory on disk.
		@since		2022-07-05 23:18:50
	**/
	public static function get_public_storage_path( $extra_path = '' )
	{
		if ( $extra_path != '' )
			$extra_path = DIRECTORY_SEPARATOR . $extra_path;
		return static::get_storage_path( 'public' ) . $extra_path;
	}

	/**
		@brief		Return the storage directory on disk.
		@since		2022-07-05 23:18:50
	**/
	public static function get_storage_path( $extra_path = '' )
	{
		if ( $extra_path != '' )
			$extra_path = DIRECTORY_SEPARATOR . $extra_path;
		return storage_path( 'app' ) . $extra_path;
	}

	/**
		@brief		Return the time this item was modified.
		@since		2019-01-05 12:43:51
	**/
	public function get_time()
	{
		// Always return the current time.
		return \Carbon\Carbon::createFromTimestamp( filemtime( $this->get_path_and_filename() ) );
	}

	/**
		@brief		Return the URL for this item.
		@since		2019-01-05 00:29:17
	**/
	public function get_url()
	{
		$route = $this->get_route();
		return route( $route, [ $this->get_filename() ] );
	}

	/**
		@brief		Return an array of the selected files.
		@since		2023-09-12 20:27:54
	**/
	public function glob( $search_for = '*' )
	{
		return glob( $search_for );
	}

	/**
		@brief		Is this a directory?
		@since		2018-12-31 16:40:29
	**/
	public function is_directory()
	{
		return true;
	}

	/**
		@brief		Is this a file?
		@since		2018-12-31 16:40:29
	**/
	public function is_file()
	{
		return false;
	}

	/**
		@brief		Is this an image?
		@since		2021-01-16 21:45:35
	**/
	public function is_image()
	{
		return false;
	}

	/**
		@brief		Normalize the filename, removing dangerous things.
		@since		2019-01-04 22:56:43
	**/
	public static function normalize( $string )
	{
		$string = str_replace( '..', '', $string );
		$string = str_replace( DIRECTORY_SEPARATOR, '', $string );
		$string = preg_replace( '/[^0-9^a-z\.A-z_-]/', '_', $string );
		$string = trim( $string );
		return $string;
	}

	/**
		@brief		Remove this directory.
		@since		2022-07-06 07:23:50
	**/
	public function remove_directory( $dir )
	{
		$dirname = File::normalize( $dir );
		$path = $this->get_path_and_filename() . DIRECTORY_SEPARATOR . $dirname;
		if ( ! is_dir( $path ) )
			return;
		rmdir( $path );
	}

	/**
		@brief		Return the sorted items.
		@since		2019-01-04 23:06:55
	**/
	public function sorted()
	{
		return $this->sortBy( function( $item )
		{
			if ( $item->is_directory() )
				$key = 'dir_';
			else
				$key = 'fil_';
			$key .= strtolower( $item->get_filename() );
			return $key;
		} );
	}

	/**
		@brief		Unlink this file from the directory.
		@since		2022-07-11 06:22:00
	**/
	public function unlink( $filename )
	{
		$file = File::from( $this->get_path() . DIRECTORY_SEPARATOR . $filename );
		if ( ! $file->exists() )
			return false;
		app()->log()->info( 'Deleting file %s', $file->get_path_and_filename() );
		$file->unlink();
		return true;
	}
}
