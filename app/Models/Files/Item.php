<?php

namespace App\Models\Files;

/**
	@brief		A file or dir.
	@since		2018-12-31 16:39:32
**/
abstract class Item
	extends \App\Classes\Collections\Collection
{
	/**
		@brief		The filename.
		@since		2019-01-03 16:13:04
	**/
	public $filename;

	/**
		@brief		Size of the file. Dirs = 0;
		@since		2019-01-03 16:13:11
	**/
	public $filesize = 0;

	/**
		@brief		From what "url" was this item created?
		@since		2022-07-05 22:18:01
	**/
	public $from = '';

	/**
		@brief		The internal path to the item.
		@since		2019-01-04 21:37:38
	**/
	public $path;

	/**
		@brief		Is this item in the private section?
		@since		2019-01-05 00:32:28
	**/
	public $private = false;

	/**
		@brief		Does this item exist on disk?
		@since		2022-07-11 06:30:01
	**/
	public abstract function exists();

	/**
		@brief		Create an item with this constructor.
		@since		2022-07-11 06:52:12
	**/
	public abstract static function from( $path );

	/**
		@brief		Return the filename.
		@since		2019-01-04 21:29:07
	**/
	public function get_filename()
	{
		return $this->filename;
	}

	/**
		@brief		Return the internal path of the file.
		@since		2019-01-04 21:35:14
	**/
	public function get_path( $subdir = '' )
	{
        if ( $subdir != '' )
            $subdir = DIRECTORY_SEPARATOR . $subdir;
		return $this->path . $subdir;
	}

	/**
		@brief		Return the disk path + filename.
		@since		2019-01-04 19:04:32
	**/
	public function get_path_and_filename()
	{
		return $this->get_path() . DIRECTORY_SEPARATOR . $this->get_filename();
	}

	/**
		@brief		Return the relative URL.
		@since		2019-01-03 18:34:24
	**/
	public function get_public_path_and_filename()
	{
		$r =  $this->get_browsable_path() . DIRECTORY_SEPARATOR . $this->get_filename();
		$r = trim( $r, '/' );
		return $r;
	}

	/**
		@brief		Return the path of this item, as visible by visitors.
		@since		2019-01-03 17:01:51
	**/
	public function get_browsable_path()
	{
		$r = str_replace( storage_path(), '', $this->get_path() );
		$r = str_replace( DIRECTORY_SEPARATOR . 'app', '', $r );
		$r = str_replace( DIRECTORY_SEPARATOR . 'public', '', $r );
		$r = str_replace( DIRECTORY_SEPARATOR . 'private', '', $r );
		$r = trim( $r, DIRECTORY_SEPARATOR );
		return $r;
	}

	/**
		@brief		Return the route used to generate this file's url.
		@since		2019-01-05 00:29:47
	**/
	public function get_route()
	{
		if ( $this->is_private() )
			return 'private_get';
		return 'public_get';
	}

	/**
		@brief		Return the time this item was modified.
		@since		2019-01-05 12:43:51
	**/
	public function get_time()
	{
		$time = fileatime( $this->get_path_and_filename() );
		return \Carbon\Carbon::createFromTimestamp( $time );
	}

	/**
		@brief		Return the URL for this item.
		@since		2019-01-05 00:29:17
	**/
	public abstract function get_url();

	/**
		@brief		Is this a directory?
		@since		2018-12-31 16:40:29
	**/
	public abstract function is_directory();

	/**
		@brief		Is this a file?
		@since		2018-12-31 16:40:29
	**/
	public abstract function is_file();

	/**
		@brief		Is this iten an image?
		@since		2022-07-06 07:32:42
	**/
	public abstract function is_image();

	/**
		@brief		Is this item marked private?
		@since		2019-01-05 00:32:50
	**/
	public function is_private()
	{
		return $this->private;
	}

	/**
		@brief		Normalize the filename, removing dangerous things.
		@since		2019-01-04 22:56:43
	**/
	public abstract static function normalize( $string );

	/**
		@brief		Set the item to private mode.
		@since		2019-01-05 00:30:22
	**/
	public function set_private()
	{
		$this->private = true;
	}
}
