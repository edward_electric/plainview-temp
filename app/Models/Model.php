<?php

namespace App\Models;

/**
	@brief		Base model class.
	@since		2018-12-31 15:40:27
**/
class Model
	extends \Illuminate\Database\Eloquent\Model
{
    /**
    	@brief		Return the name of the table this model uses.
    	@details	Why is this not standard??
    	@since		2018-12-27 12:12:36
    **/
	public static function getTableName()
	{
		return with( new static() )->getTable();
	}
}
