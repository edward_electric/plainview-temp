<?php

namespace App\Models\Alerts;

/**
	@brief		A good alert.
	@since		2018-07-12 22:52:55
**/
class Success
	extends Alert
{
	/**
		@brief		Return the type of alert this is.
		@since		2018-07-12 22:54:25
	**/
	public function get_type()
	{
		return 'success';
	}
}
