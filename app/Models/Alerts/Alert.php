<?php

namespace App\Models\Alerts;

/**
	@brief		A basic alert type.
	@since		2018-07-12 22:52:34
**/
class Alert
{
	/**
		@brief		The message to be displayed to the user.
		@since		2018-07-12 23:20:41
	**/
	public $message;

	/**
		@brief		Return our assembled HTML.
		@since		2018-07-12 23:09:21
	**/
	public function get_html()
	{
		return view( 'components.alerts.alert', [
			'message' => $this->get_message(),
			'type' => $this->get_type(),
		] );
	}

	/**
		@brief		Return our message.
		@since		2018-07-12 23:10:03
	**/
	public function get_message()
	{
		return $this->message;
	}

	/**
		@brief		Return the type of alert this is.
		@since		2018-07-12 22:54:25
	**/
	public function get_type()
	{
		return 'alert';
	}

	/**
		@brief		Set the message of this alert.
		@since		2018-07-12 23:20:24
	**/
	public function set_message( $message )
	{
		$this->message = $message;
		return $this;
	}
}
