<?php

return [

    /**
    	@brief		After how many seconds to delete uploaded files.
    	@details	Default is 2 days.
    	@since		2019-01-05 18:37:33
    **/
    'file_delete_age' => env( 'TEMP_FILE_DELETE_AGE', 60 * 60 * 24 * 2 ),
];
