<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware( [ '\\App\\Http\\Middleware\\Cleanup' ] )->group( function()
{
    Route::get( '/', function()
    {
        return redirect( 'public/' );
    } );

	// Root does nothing but redirect to public.
	Route::middleware( [ '\\App\\Http\\Middleware\\Public_Exists' ] )
		->prefix( 'public' )
		->group( function()
    {
        Route::get( '/', 'Public_Files@get' )->name( 'index' );
		Route::get( '/{any}', 'Public_Files@get' )->name( 'public_get' );
		Route::post( '/', 'Public_Files@post' );
    } );

	Route::middleware( [ '\\App\\Http\\Middleware\\Private_Exists' ] )
		->prefix( 'private' )
		->group( function()
	{
		Route::get( '/', 'Private_Files@get' );
		Route::get( '{any}', 'Private_Files@get' )
			->name( 'private_get' )
			->where( 'any', '.*' );						// Include all subdirectories and everything.
		Route::post( '{any}', 'Private_Files@post' )
			->name( 'private_post' )
			->where( 'any', '.*' );						// Include all subdirectories and everything.
	} );

	Route::get( '/create_private', 'Private_Files@create' )->name( 'private_create' );
} );
