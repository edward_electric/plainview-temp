# Plainview Temp

This is a PHP script to allow upload and temporary storage of files and texts for easy sharing.

## Features

- Anyone can upload files
- Anyone can download files
- Texts can be uploaded
- Files can be uploaded to private / hidden directories
- Private files can only be downloaded if the URL is known

# Requirements

- PHP 7.1
- Apache or Nginx or similar

The storage/app directory is used for file storage.

# Installation

- Download the script.
- Unzip the file to a directory.
- Point your server to use the public/ directory as the root.

# Usage

The script will automatically delete files older than 2 days. The setting can be changed by a .env setting:

Set the file delete age to 3600 seconds (1 hour)
`TEMP_FILE_DELETE_AGE=3600`

# Git

The project's git is https://bitbucket.org/edward_electric/plainview-temp/src

# Contact

The project's home page is: https://plainviewplugins.com/plainview-temp/
The author can be contacted at edward@plainview.se
