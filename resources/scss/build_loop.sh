#!/bin/bash

if [[ "$1" == "" ]]; then
	echo Please specify a config file to use.
	exit
fi

./build.sh "$1"

echo "Beginning stat loop for $1"

function collect_stats()
{
	echo `ls -ltrR --full-time`
}

OLD_STATS=`collect_stats`
while true; do
	NEW_STATS=`collect_stats`
	if [ "$NEW_STATS" != "$OLD_STATS" ] ; then
		echo -n "Rebuilding... "
		./build.sh "$1"
		echo "`date` Done."
		OLD_STATS=`collect_stats`
	fi
    sleep 1
done
