/**
	@brief		Create and display a gallery.
	@since		2021-01-17 12:53:43
**/
jQuery( document ).ready( function( $ )
{
	var $carousel = $( '#gallery_carousel' );
	var $gallery_modal = $( '#gallery_modal' );

	/**
		@brief		Load the image.
		@since		2021-01-17 17:56:30
	**/
	function activate_gallery_image( item )
	{
		if ( ! item )
			return;

		var $item = $( item );
		var $img = $( 'img', $item );

		// No src? Not loaded.
		if ( ! $img.attr( 'src' ) )
		{
			var src = $img.attr( '_src' );
			console.log( 'Activating', src );
			$img.attr( 'src', src );
		}
	}

	$gallery_modal.on( 'show.bs.modal', function()
	{
		// Only init the carousel once.
		if ( $gallery_modal.hasClass( 'carousel_ready' ) )
		{
			// Select the first image.
			$carousel.carousel( 0 );
			return;
		}
		$gallery_modal.addClass( 'carousel_ready' );

		var $carousel_inner = $( '.carousel-inner' );

		for ( var counter = 0; counter < gallery_images.length; counter++ )
		{
			var $image = $( '<img class="d-block w-100">' );
			$image.prop( 'src', gallery_images[ counter ] );

			var $item = $( '<div class="carousel-item">' );
			$image.appendTo( $item );

			if ( counter == 0 )
				$item.addClass( "active" );

			$item.appendTo( $carousel_inner );
		}

		// Automatically activate the first item.
		var $items = $( '.carousel-item', $carousel );
		activate_gallery_image( $items.get( 0 ) );

		// And after a small delay, the next one.
		setTimeout( function()
		{
			activate_gallery_image( $items.get( 1 ) );
		}, 1000 );
	} );

	$carousel.on( 'slide.bs.carousel', function ( data )
	{
		activate_gallery_image( data.relatedTarget );

		// Try activate the next one also.
		setTimeout( function()
		{
			var item = $( '.carousel-item', $carousel ).get( data.to + 1 );
			activate_gallery_image( item );
		}, 1000 );
  	} );
} );
