jQuery( document ).ready( function( $ )
{
	var $csrf_token = $('meta[name="csrf-token"]');
	var csrf_token = $csrf_token.attr( 'content' );

	// Single upload form
	var $upload_form = $( '#upload_form' );
	$upload_form.hide();

	var $plupload = $( '.plupload' );
	$plupload.removeClass( 'd-none' );
	var $files_list = $( '.files_list', $plupload );
	var $select_files = $( '.select_files', $plupload );
	var $upload_button = $( '.input_upload.btn', $plupload );

	var uploader = new plupload.Uploader(
	{
		'browse_button' : $select_files[ 0 ],
		'headers' :
		{
			'X-CSRF-TOKEN' : csrf_token,
		},
		'multipart_params' :
		{
			'upload' : true,		// We need to click the upload button.
		},
		'init':
		{
			'Error': function(up, err)
			{
				$( '.errors .col', $plupload ).append( "\nError #" + err.code + ": " + err.message + '<br/>' );
			},

			'FilesAdded': function(up, files)
			{
				$select_files.hide();

				plupload.each(files, function(file)
				{
					var $template = $( '.plupload_file.template' )
						.clone()
						.removeClass( 'd-none' )
						.removeClass( 'template' );
					$template.attr( 'id', file.id );
					$( '.filename', $template ).html( file.name );
					$( '.filesize', $template ).html( plupload.formatSize(file.size) );
					$template.appendTo( $files_list );
				});

				// Automatically start the upload.
				uploader.start();
			},

			'UploadComplete' : function()
			{
				document.location = document.location;
			},

			'UploadProgress': function(up, file)
			{
				var $file = $( '#' + file.id );
				var percentage = file.percent + '%';
				$( '.progress-bar', $file )
					.css( 'width', percentage )
					.html( percentage );
			},
		},
		'runtimes' : 'html5',
		'url' : $upload_form.attr( 'action' ),
	} );

	uploader.init();
} );
