jQuery( document ).ready( function( $ )
{
	var files_table = function()
	{
		var $$ = this;
		this.$csrf_token = $('meta[name="csrf-token"]');
		this.csrf_token = this.$csrf_token.attr( 'content' );

		this.$files_table = $( 'table.files' );

		// Delete button.
		this.$delete_button = false;

		// Get the checkboxes.
		this.$head_cb = $( 'th.checkbox input', this.$files_table );
		this.$body_cb = $( 'td.checkbox input', this.$files_table );

		/**
			@brief		Return an array of all selected files.
			@since		2022-07-10 22:41:04
		**/
		this.get_selected_files = function()
		{
			var r = [];
			$.each( this.$body_cb, function( index, item )
			{
				var $item = $( item );
				if ( ! $item.prop( 'checked' ) )
					return;
				r.push( $item.prop( 'value' ) );
			} );
			return r;
		}

		this.init_checkboxes = function()
		{
			var that = this;

			$( '.checkbox.column', that.$files_table ).removeClass( 'd-none' );

			// Make the header checkbox select / unselect them all.
			this.$head_cb.change( function()
			{
				var value = $( this ).prop( 'checked' );
				that.$body_cb.prop( 'checked', value );
				that.show_or_hide_buttons_for_checkboxes();
			} ).change();

			that.$body_cb.change( function()
			{
				that.show_or_hide_buttons_for_checkboxes();
			} );
		}

		this.init_delete_button = function()
		{
			// Find the delete button div.
			this.$delete_button_div = $( '.delete_button' ).removeClass( 'd-none' );
			// And now the button.
			this.$delete_button = $( 'button', this.$delete_button_div );

			var caller = this;
			this.$delete_button.click( function()
			{
				console.debug( 'Submit the delete' );
				$.ajax( {
					'data' : {
						'delete_files' : {
							'files_to_delete' : caller.get_selected_files(),
						},
					},
					'dataType' : 'json',
					'method' : 'POST',
				} )
				.always( function( data )
				{
					console.debug( 'Always delete', data );
				} )
				.done( function( data )
				{
					// Reload the page, because files are gone.
					window.location = window.location;
				} );
			} );
		}

		/**
			@brief		Handle the zipping.
			@since		2023-09-12 11:56:07
		**/
		this.init_zip_button = function()
		{
			// Find the zip button div.
			this.$zip_button_div = $( '.zip_button' ).removeClass( 'd-none' );
			// And now the button.
			this.$zip_button = $( 'button', this.$zip_button_div );

			var caller = this;
			this.$zip_button.click( function()
			{
				console.debug( 'Submit the zip' );
				$.ajax( {
					'data' : {
						'zip_files' : {
							'files_to_zip' : caller.get_selected_files(),
						},
					},
					'dataType' : 'html',
					'method' : 'POST',
				} )
				.always( function( data )
				{
					console.debug( 'Always zip', data );
				} )
				.done( function( data )
				{
					// Reload the page, because a zip was created.
					window.location = window.location;
				} );
			} );
		}

		this.init = function()
		{
			this.init_delete_button();		// Needs to be called before checkboxes due to show hide.
			this.init_zip_button();			// Needs to be called before checkboxes due to show hide.
			this.init_checkboxes();
		}

		/**
			@brief		Decide whether to show or hide the various checkbox connected buttons.
			@since		2022-07-10 22:41:19
		**/
		this.show_or_hide_buttons_for_checkboxes = function()
		{
			var show = this.get_selected_files().length;
			if ( show )
			{
				$( '.button_for_checkboxes' ).show();
			}
			else
			{
				$( '.button_for_checkboxes' ).hide();
			}
		}
	}

	var ft = new files_table();
	ft.init();
} );
