@if ( $files->get_images()->count() > 0 )
	<div class="col">
		<div class="text-center gallery_link pb-2">
			<div class="btn btn-secondary gallery" data-bs-toggle="modal" data-bs-target="#gallery_modal">
				<i class="fas fa-images"></i>
				Open gallery
			</div>
		</div>
	</div>
@endif

<script type="text/javascript">
	var gallery_images = {!! json_encode( $files->get_image_urls() ) !!};
</script>
<div class="modal fade" id="gallery_modal" tabindex="-1" role="dialog" aria-labelledby="gallery_modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div id="gallery_carousel" class="carousel slide"  data-bs-ride="false">
					<div class="carousel-inner">
					</div>
					<button class="carousel-control-prev" type="button" data-bs-target="#gallery_carousel" role="button" data-bs-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</button>
					<button class="carousel-control-next" type="button" data-bs-target="#gallery_carousel" role="button" data-bs-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
