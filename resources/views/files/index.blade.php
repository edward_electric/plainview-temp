@if ( $files->is_private() )
	@section( 'h1_icon', 'lock' )
@else
	@section( 'h1_icon', 'unlock' )
@endif

@section( 'h1', basename( $files->get_public_path_and_filename() ) )

@extends( 'layouts.default' )

@section( 'content' )

	<div class="utilities row">
		@if ( $files->is_private() )
		<div class="col back_to_public text-center">
			<a class="btn btn-secondary" href="{{ route( 'index' ) }}"><i class="fa fa-unlock"></i> Back to public</a>
		</div>
		@endif
		@if ( $files->get_images()->count() > 0 )
		<div class="col">
			@include( 'files.gallery' )
		</div>
		@endif
		<div class="col">
			<div class="create_private_upload text-center pb-2">
				<a class="btn btn-secondary" href="{{ route( 'private_create' ) }}">
					<i class="fa fa-lock"></i>
					Create a new private upload
				</a>
			</div>
		</div>
	</div>

	@if ( count( $files ) > 0 )
		@include( 'files.files' )
	@else
		@include( 'files.empty' )
	@endif

	<div class="forms">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
		@foreach( $forms as $form_id => $form )
			<li class="nav-item" role="presentation">
				@if ($loop->first)
					<button class="nav-link active" id="{{ $form_id }}_tab" data-bs-toggle="tab" data-bs-target="#tab_content_{{ $form_id }}" type="button" role="tab" aria-selected="true">
						@include( 'files.tabicon' )
						{{ $form->human_name }}
					</button>
				@else
					<button class="nav-link" id="{{ $form_id }}_tab" data-bs-toggle="tab" data-bs-target="#tab_content_{{ $form_id }}" role="tab" type="button">
						@include( 'files.tabicon' )
						{{ $form->human_name }}
					</button>
				@endif
			</li>
		@endforeach
		</ul>
		<div class="tab-content" id="myTabContent">
		@foreach( $forms as $form_id => $form )
			@if ($loop->first)
				<div class="tab-pane fade show active" id="tab_content_{{ $form_id }}" role="tabpanel">
			@else
				<div class="tab-pane fade" id="tab_content_{{ $form_id }}" role="tabpanel">
			@endif
				@if ( $form_id == 'upload' )
					@include( 'files.plupload' )
				@endif
				{!! $form !!}
			</div>
		@endforeach
		</div>
	</div>

@append
