<table class="files table table-striped table-responsive-md">
	<thead>
		<tr>
			<th class="checkbox column d-none">
				<input type="checkbox" class="form-check-input" />
			</th>
			<th>Filename</th>
			<th>Size</th>
			<th>Date</th>
		</tr>
	</thead>
	<tbody>
		@foreach( $files->sorted() as $file )
		<tr>
			<td class="checkbox column d-none">
				<input type="checkbox" class="form-check-input" value="{{ $file->get_filename() }}"/>
			</td>
			@include( 'files.file' )
			<td title="{{ $file->get_time()->toDateTimeString() }}">
				{{ $file->get_time()->diffForHumans() }}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<p class="zip_button d-none button_for_checkboxes">
	<button class="btn btn-secondary zip">
        <i class="fa fa-compress"></i>
        {{ __( 'Zip selected' ) }}
    </button>
</p>

<p class="delete_button d-none button_for_checkboxes">
	<button class="btn btn-warning delete">
        <i class="fa fa-trash"></i>
        {{ __( 'Delete selected' ) }}
    </button>
</p>

</div>
