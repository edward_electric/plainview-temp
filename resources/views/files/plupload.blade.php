<div class="plupload p-2 pb-4 pt-4 d-none">
	<div class="plupload_file template row d-none">
		<div class="col-4 filename">
		</div>
		<div class="col upload_progress">
		<div class="progress">
			<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
		</div>
		</div>
		<div class="col-4 filesize">
		</div>
	</div>
	</template>
	<div class="row">
		<div class="col">
			<div class="row uploader">
				<div class="col-12 text-center">
					<div class="btn btn-primary select_files">
                        <i class="fa fa-upload"></i>
                        {{ __( 'Select files' ) }}
                    </div>
				</div>
				<div class="col-12 text-center files_list">
				</div>
			</div>
			<div class="row errors">
				<div class="col">
				</div>
			</div>
		</div>
	</div>
</div>
