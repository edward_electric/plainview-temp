<header>
	<h1>
		<a href="{{ url()->full() }}">
		@hasSection ( 'h1_icon' )
			<i class="fa fa-@yield( 'h1_icon' )"></i>
		@endif
		@yield( 'h1' )
		</a>
	</h1>
</header>
