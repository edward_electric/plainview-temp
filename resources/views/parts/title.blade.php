@php
	$app_name = config( 'app.name' );
@endphp
@hasSection( 'h1' )
	@yield( 'h1' ) | {{ $app_name }}
@else
	{{ $app_name }}
@endif
