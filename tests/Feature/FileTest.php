<?php

namespace Tests\Feature;

use App\Models\Files\Directory;
use App\Models\Files\File;

/**
	@brief		Test file handling.
	@since		2022-07-06 14:13:27
**/
class FileTest extends TestCase
{
	/**
		@brief		Test a public file.
		@since		2022-07-06 14:13:39
	**/
	public function test_public_file()
	{
		// First a file in the public directory.
		$filename = md5( microtime() ) . '.txt';
		$filepath = Directory::get_public_storage_path( $filename );

		$this->assertFalse( file_exists( $filepath ) );
		touch( $filepath );
		$this->assertTrue( file_exists( $filepath ) );

		$f = File::from( $filepath );

		$this->assertEquals( $f->get_filename(), $filename );
		$this->assertEquals( $f->get_browsable_path(), '' );
		$this->assertEquals( $f->get_path(), dirname( $filepath ) );
		$this->assertEquals( $f->get_url(), route( 'public_get', [ $filename ] ) );

		$f->unlink();
		$this->assertFalse( file_exists( $filepath ) );
	}

	/**
		@brief		Test a private file.
		@since		2022-07-06 14:13:39
	**/
	public function test_private_file()
	{
		$directory_name = md5( microtime() );
		$filename = md5( microtime() ) . '.txt';
		$filepath = Directory::get_private_storage_path( $directory_name . DIRECTORY_SEPARATOR . $filename );
		$directory_path = Directory::get_private_storage_path( $directory_name );

		$this->assertFalse( is_dir( $directory_path ) );
		mkdir( $directory_path );
		$this->assertTrue( is_dir( $directory_path ) );

		$this->assertFalse( file_exists( $filepath ) );
		touch( $filepath );
		$this->assertTrue( file_exists( $filepath ) );

		$f = File::from( $filepath );

		$this->assertEquals( $f->get_filename(), $filename );
		$this->assertEquals( $f->get_browsable_path(), $directory_name );
		$this->assertEquals( $f->get_path(), dirname( $filepath ) );
		$this->assertEquals( $f->get_url(), route( 'private_get', [ $directory_name . DIRECTORY_SEPARATOR . $filename ] ) );

		$f->unlink();
		$this->assertFalse( file_exists( $filepath ) );

		rmdir( $directory_path );
		$this->assertFalse( is_dir( $directory_path ) );
	}
}
