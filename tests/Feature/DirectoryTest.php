<?php

namespace Tests\Feature;

use App\Models\Files\Directory;

/**
	@brief		Test directory handling.
	@since		2022-07-05 23:06:53
**/
class DirectoryTest extends TestCase
{

	/**
		@brief		Test create and remove directories.
		@since		2022-07-06 07:24:59
	**/
	public function test_create_and_remove()
	{
		$dirname = md5( microtime() );
		$created_dir = Directory::get_public_storage_path( $dirname );

		$dir = Directory::from( Directory::get_public_storage_path() );

		$this->assertFalse( is_dir( $created_dir ) );
		$dir->create_directory( $dirname );
		$this->assertTrue( is_dir( $created_dir ) );

		$dir->remove_directory( $dirname );
		$this->assertFalse( is_dir( $created_dir ) );
	}

	/**
		@brief		Test the is queries.
		@since		2022-07-06 07:19:21
	**/
	public function test_is()
	{
		$dirname = md5( microtime() );
		$dir = Directory::from( $dirname );

		$this->assertTrue( $dir->is_directory() );
		$this->assertFalse( $dir->is_file() );
	}

	/**
		@brief		Test autocreation of private directories.
		@since		2022-07-05 23:24:08
	**/
	public function test_private_autocreate()
	{
		$dirname = md5( microtime() );
		$private = Directory::get_private_storage_path( $dirname );
		$this->assertFalse( is_dir( $private ) );

		\App\Http\Middleware\File_Exists::check_file( 'private', '/private/' . $dirname );
		$this->assertTrue( is_dir( $private ) );

		rmdir( $private );
	}

	/**
		@brief		Test not being allowed to access the base private dir.
		@since		2022-07-05 23:58:49
	**/
	public function test_private_empty()
	{
		$r = \App\Http\Middleware\File_Exists::check_file( 'private', '/private/' );
		$this->assertFalse( $r );
	}

	/**
		@brief		Test the private URL.
		@since		2022-07-05 23:56:13
	**/
	public function test_private_url()
	{
		$dirname = md5( microtime() );
		$dir = Directory::from( $dirname );
		$dir->set_private( true );
		$this->assertEquals( route( 'private_get', [ $dirname ] ), $dir->get_url() );
	}


	/**
		@brief		Test a public file.
		@since		2022-07-05 23:59:49
	**/
	public function test_public_200()
	{
		$filename = md5( microtime() ) . '.txt';
		$filepath = Directory::get_public_storage_path( $filename );
		touch( $filepath );
		$r = \App\Http\Middleware\File_Exists::check_file( 'public', '/public/' . $filename );
		$this->assertTrue( is_object( $r ) );
		unlink( $filepath );
	}

	/**
		@brief		Test a missing public file.
		@since		2022-07-05 23:59:49
	**/
	public function test_public_404()
	{
		$filename = md5( microtime() );
		$r = \App\Http\Middleware\File_Exists::check_file( 'public', '/public/' . $filename );
		$this->assertFalse( $r );
	}

	/**
		@brief		Test the public URL.
		@since		2022-07-05 23:56:13
	**/
	public function test_public_url()
	{
		$dirname = md5( microtime() );
		$dir = Directory::from( $dirname );
		$this->assertEquals( route( 'public_get', [ $dirname ] ), $dir->get_url() );
	}

	/**
		@brief		Test autocreation of public directories - which it won't do.
		@since		2022-07-05 23:24:08
	**/
	public function test_public_autocreate()
	{
		$dirname = md5( microtime() );
		$public = Directory::get_public_storage_path( $dirname );
		$this->assertFalse( is_dir( $public ) );

		\App\Http\Middleware\File_Exists::check_file( 'public', '/public/' . $dirname );
		$this->assertFalse( is_dir( $public ) );
	}

	/**
		@brief		Test the storage paths.
		@since		2022-07-05 23:21:17
	**/
	public function test_storage_paths()
	{
		$real_path = storage_path( 'app' );

		// Test the public dir.
		$queried_path = Directory::get_public_storage_path();
		$this->assertEquals( $real_path . DIRECTORY_SEPARATOR . 'public',
			$queried_path
		);

		// Test the private dir.
		$queried_path = Directory::get_private_storage_path();
		$this->assertEquals( $real_path . DIRECTORY_SEPARATOR . 'private',
			$queried_path
		);
	}

	/**
		@brief		Test the time.
		@since		2022-07-06 07:19:56
	**/
	public function test_time()
	{
		$dirname = md5( microtime() );
		$dir = Directory::from( Directory::get_public_storage_path() );
		$dir->create_directory( $dirname );
		$time = time();
		$this->assertEquals( $dir->get_time()->getTimestamp(), $time );
		$dir->remove_directory( $dirname );
	}

	/**
		@brief		Test unlinking of a file.
		@since		2022-07-11 06:23:05
	**/
	public function test_unlink()
	{
		$filename = md5( microtime() ) . '.txt';
		$filepath = Directory::get_public_storage_path( $filename );

		$this->assertFalse( file_exists( $filepath ) );
		touch( $filepath );
		$this->assertTrue( file_exists( $filepath ) );

		$dir = new Directory( dirname( $filepath ) );
		$dir->unlink( $filename );
		$this->assertFalse( file_exists( $filepath ) );
	}
}
